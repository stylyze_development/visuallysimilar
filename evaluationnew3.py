import csv
import numpy as np
import pandas as pd
import pickle
import re

import vsnm.evaldatanew as data
import vsnm.eval2new3 as model
import vsnm.configurationnew as configuration
 

# TODO: implement setup.py/requirement.txt and package all the
# modulues used here


def main():

    args = configuration.parse_arguments()

    model_input_path = data.load_data(args["database_path"],
                                      args["output_table_path"],
                                      args["column_names"])

    model.embedding(model_input_path)

if __name__ == '__main__':
    main()
