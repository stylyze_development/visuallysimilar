import csv
import numpy as np
import pandas as pd
import pickle
import re
from datetime import datetime

import vsnm.querynew as query


def main(): 
	startTime = datetime.now()
	image_paths = ["/opt/tomcat/blobs/neimanmarcus//5b2a7295e4b02087999363b5/5b2a7295e4b02087999363b3.jpg",
				   "/opt/tomcat/blobs/neimanmarcus//5b2a797be4b02087999392b1/5b2a797be4b02087999392b0.jpg"]
	vs, url, score = query.query(image_paths)
	time_used = datetime.now() - startTime
	print("Time used (total): " + str(time_used))
	# print(vs)
	# print(score)
	table = pd.DataFrame(vs, columns=[
                         '1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
                         '11', '12', '13', '14', '15', '16', '17', '18', '19', '20'])
	table.to_csv("/home/tfuser/visuallysimilar/visually_similar_v1/output_tables/" + "id.csv")
	table = pd.DataFrame(url, columns=[
                         '1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
                         '11', '12', '13', '14', '15', '16', '17', '18', '19', '20'])
	table.to_csv("/home/tfuser/visuallysimilar/visually_similar_v1/output_tables/" + "url.csv")

     
if __name__ == '__main__':
    main()