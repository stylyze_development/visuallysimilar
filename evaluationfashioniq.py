import csv
import numpy as np
import pandas as pd
import pickle
import re

import vsnm.evaldatafashioniq as data
import vsnm.configurationfashioniq as configuration
 

def main():

    args = configuration.parse_arguments()

    data.load_data(args["database_path"],args["column_names"])

     
if __name__ == '__main__':
    main()
