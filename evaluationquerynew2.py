import csv
import numpy as np
import pandas as pd
import pickle
import re
from datetime import datetime
import os
import json
import time

import vsnm.querynew2 as query
 
import vsnm.modelsnew as models

import tensorflow as tf

cwd = os.getcwd()


def main():
	init = tf.global_variables_initializer()
	with tf.Session() as sess:

		# embedding_size = 25088
		# batch_size = 1
		# output_layer = "relu5_4"
		img_placeholder = tf.placeholder(tf.float32, shape=(None, 112, 112, 3))
		net_val, mean_pixel = models.vgg19_no_top("/home/tfuser/visuallysimilar/visually_similar_v1//weights/imagenet-vgg-verydeep-19.mat", img_placeholder)
		# matrix_res_final = np.zeros([len(image_paths), embedding_size])
		# norm_final = np.zeros([len(image_paths), 1])

		sess.run(init)

		startTime0 = datetime.now()

		startTime = datetime.now()
		# with open(cwd + "/embeddings", "rb") as my_file2:
		# 	matrix_res = pickle.load(my_file2)

		# with open(cwd + "/norms", "rb") as my_file2:
		# 	norms = pickle.load(my_file2)    

		with open(cwd + "/output_tables/deep_learning_df", "rb") as my_file2:
			deep_learning_df = pickle.load(my_file2)
		time_used = datetime.now() - startTime
		print("Time used (load files): " + str(time_used))

		while (True):

			while (os.path.isfile("/home/tfuser/visuallysimilar/visually_similar_v1/test_request.json")):
				startTime1 = datetime.now()
				time.sleep(.200)
				with open("/home/tfuser/visuallysimilar/visually_similar_v1/test_request.json") as json_file:
					data = json.load(json_file)
					image_pathss = [data]

				startTime = datetime.now()
				with open(cwd + "/embeddings", "rb") as my_file2:
					matrix_res = pickle.load(my_file2)

				with open(cwd + "/norms", "rb") as my_file2:
					norms = pickle.load(my_file2)
				time_used = datetime.now() - startTime
				print("************************")
				print("Time used (load files): " + str(time_used))

				# image_pathss = ["/opt/tomcat/blobs/neimanmarcus//5b2a7295e4b02087999363b5/5b2a7295e4b02087999363b3.jpg",
				# 			   "/opt/tomcat/blobs/neimanmarcus//5b2a797be4b02087999392b1/5b2a797be4b02087999392b0.jpg",
				# 			   "/opt/tomcat/blobs/neimanmarcus//5b2a7295e4b02087999363b5/5b2a7295e4b02087999363b3.jpg",
				# 			   "/opt/tomcat/blobs/neimanmarcus//5b2a797be4b02087999392b1/5b2a797be4b02087999392b0.jpg",
				# 			   "/opt/tomcat/blobs/neimanmarcus//5b2a7295e4b02087999363b5/5b2a7295e4b02087999363b3.jpg",
				# 			   "/opt/tomcat/blobs/neimanmarcus//5b2a797be4b02087999392b1/5b2a797be4b02087999392b0.jpg",
				# 			   "/opt/tomcat/blobs/neimanmarcus//5b2a7295e4b02087999363b5/5b2a7295e4b02087999363b3.jpg",
				# 			   "/opt/tomcat/blobs/neimanmarcus//5b2a797be4b02087999392b1/5b2a797be4b02087999392b0.jpg",
				# 			   "/opt/tomcat/blobs/neimanmarcus//5b2a7295e4b02087999363b5/5b2a7295e4b02087999363b3.jpg",
				# 			   "/opt/tomcat/blobs/neimanmarcus//5b2a797be4b02087999392b1/5b2a797be4b02087999392b0.jpg",
				# 			   "/opt/tomcat/blobs/neimanmarcus//5b2a7295e4b02087999363b5/5b2a7295e4b02087999363b3.jpg",
				# 			   "/opt/tomcat/blobs/neimanmarcus//5b2a797be4b02087999392b1/5b2a797be4b02087999392b0.jpg",
				# 			   "/opt/tomcat/blobs/neimanmarcus//5b2a7295e4b02087999363b5/5b2a7295e4b02087999363b3.jpg",
				# 			   "/opt/tomcat/blobs/neimanmarcus//5b2a797be4b02087999392b1/5b2a797be4b02087999392b0.jpg",
				# 			   "/opt/tomcat/blobs/neimanmarcus//5b2a7295e4b02087999363b5/5b2a7295e4b02087999363b3.jpg",
				# 			   "/opt/tomcat/blobs/neimanmarcus//5b2a797be4b02087999392b1/5b2a797be4b02087999392b0.jpg",
				# 			   "/opt/tomcat/blobs/neimanmarcus//5b2a7295e4b02087999363b5/5b2a7295e4b02087999363b3.jpg",
				# 			   "/opt/tomcat/blobs/neimanmarcus//5b2a797be4b02087999392b1/5b2a797be4b02087999392b0.jpg",
				# 			   "/opt/tomcat/blobs/neimanmarcus//5b2a7295e4b02087999363b5/5b2a7295e4b02087999363b3.jpg",
				# 			   "/opt/tomcat/blobs/neimanmarcus//5b2a797be4b02087999392b1/5b2a797be4b02087999392b0.jpg",
				# 			   "/opt/tomcat/blobs/neimanmarcus//5b2a7295e4b02087999363b5/5b2a7295e4b02087999363b3.jpg",
				# 			   "/opt/tomcat/blobs/neimanmarcus//5b2a797be4b02087999392b1/5b2a797be4b02087999392b0.jpg"]

				for image_path in image_pathss: 

					image_paths = [image_path]
					startTime = datetime.now()
					grouped = deep_learning_df.groupby(["RETAILER_TYPE"])
					time_used = datetime.now() - startTime
					print("Time used (group by): " + str(time_used))

					vs, url, score = query.query(image_paths, matrix_res, norms, deep_learning_df, grouped, img_placeholder, net_val)
					# time_used = datetime.now() - startTime1
					# print("Time used (per lsb): " + str(time_used))

					# time_used = datetime.now() - startTime0
					# print("Time used (total): " + str(time_used))
					print(vs)
					# print(score)

					table = pd.DataFrame(vs, columns=[
										 '1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
										 '11', '12', '13', '14', '15', '16', '17', '18', '19', '20'])
					table.to_csv("/home/tfuser/visuallysimilar/visually_similar_v1/output_tables/" + "id.csv")
					# table = pd.DataFrame(url, columns=[
					# 					 '1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
					# 					 '11', '12', '13', '14', '15', '16', '17', '18', '19', '20'])
					# table.to_csv("/home/tfuser/visuallysimilar/visually_similar_v1/output_tables/" + "url.csv")
					os.remove("/home/tfuser/visuallysimilar/visually_similar_v1/test_request.json")
					time_used = datetime.now() - startTime1
					print("Time used (per lsb): " + str(time_used))

	 
if __name__ == '__main__':
	main()