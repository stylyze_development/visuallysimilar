import sys

# TODO: use python argparse to manage the configuration file
# from command line

defaults_config = {"n_H0": 112,
                   "n_W0": 112,
                   "n_C0": 3,
                   "batch_size": 1,
                   "training_ratio": 1.00,
                   # "embedding_size": 4096,
                   "embedding_size": 25088,
                   # "embedding_size": 100352,
                   "net_3_dim": 4096,
                   "search_size": 31,
                   "search_size_2": 21,
                   "image_normalization": 250.,
                   "same_image_epsilon": 0.00000001,
                   "num_epoch": 3,
                   "MARGIN": 10,
                   "learning_rate": 0.0001,
                   "keepratio": 1.0,
                   "model_name": "vgg19",
                   # "model_name": "triplet",
                   # "output_layer": "relu7",
                   "output_layer": "relu5_4",
                   # "output_layer": "pool5",
                   # "database_path": sys.argv[1],
                   "database_path": "/data_tables/Visually_Similar_neimanmarcus_qaApproved_qaUnapproved_extended_06282021.csv",
                   # "database_path": "/data_tables/VISUALLY_SIMILAR_NM_APPROVED_EXTENDED_with_Brands_20190227_CV.csv",
                   "output_table_path": "/output_tables/deep_learning_df",
                   "output_table_path_2": "/output_tables/deep_learning_dic",
                   "output_table_path_3": "/output_tables/",
                   "output_table_path_4": "/output_tables/df_triplet_final",
                   "output_table_path_5": "/output_tables/test_p.csv",
                   "output_table_path_6": "/output_tables/test_n.csv",
                   "output_table_path_7": "/data_tables/test_box_reference.csv",
                   "output_table_path_8": "/data_tables/test_box.csv",
                   "output_table_path_9": "/output_tables/deep_learning_score_sdic",
                   "output_table_path_10": "/output_tables/test_p_2.csv",
                   "output_table_path_11": "/output_tables/test_n_2.csv",
                   "output_table_path_12": "/output_tables/nm_id_score_0628202125088.json",
                   # "output_table_path_13": "/output_tables/approvedAndUnapprovedVSI_1538634741748.csv",
                   "output_table_path_13": "/output_tables/[NM]approvedAndUnapprovedVSI_1556010313238.csv",
                   "model_weight_path": "/weights/imagenet-vgg-verydeep-19.mat",
                   "model_weight_path_2": "/weights/",
                   "column_names": ["VARIANT_ID", "TYPES", "STYLES", "CURATOR_IMAGE_PATH", "IMAGE_URL", "CATEGORIES", "BRAND"]
                   }


def parse_arguments():
    args = defaults_config
    return args
