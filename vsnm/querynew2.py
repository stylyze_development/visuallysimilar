import csv
import cv2
from datetime import datetime
import logging
import numpy as np
import os
import pandas as pd
import pickle
import scipy.io
import tensorflow as tf
# import tensorflow.compat.v1 as tf
# tf.disable_v2_behavior()
from PIL import Image
from numpy import linalg as LA

from . import searchnew as search
from . import inferencenm2 as inference


def query(image_paths, matrix_res, norms, deep_learning_df, grouped, img_placeholder, net_val):
	cwd = os.getcwd()

	# startTime = datetime.now()
	# image = tf.keras.preprocessing.image
	# # preprocess = tf.keras.applications.VGG19.preprocess_input
	# # myinception = tf.keras.applications.VGG19(include_top=False, pooling='max')
	# preprocess = tf.keras.applications.inception_v3.preprocess_input
	# myinception = tf.keras.applications.inception_v3.InceptionV3(include_top=False, pooling='max')
	# time_used = datetime.now() - startTime
	# print("Time used (TF): " + str(time_used)) 

	search_size = 30
	search_size_2 = 20
	same_image_epsilon = 0.00000001

	startTime = datetime.now()
	query_res, query_norm = inference.inference(image_paths, img_placeholder, net_val)
	# print(query_res.shape)
	time_used = datetime.now() - startTime
	print("Time used (inference): " + str(time_used))

	# query_res = np.zeros([len(image_paths), 2048])
	# query_norm = np.zeros([len(image_paths), 1])
	# i = 0
	# startTime = datetime.now()
	# for image_path_i in image_paths:
	# 	img = Image.open(image_path_i)
	# 	img = img.resize((229, 229))
	# 	mat = image.img_to_array(img)
	# 	mat = np.expand_dims(mat, axis=0)
	# 	aa = preprocess(mat)
	# 	query_res[i:(i+1),:] = myinception.predict(aa)
	# 	query_norm[i:(i+1),:] = LA.norm(query_res)
	# 	i += 1
	# print(query_res.shape)
	# time_used = datetime.now() - startTime
	# print("Time used (inference): " + str(time_used)) 

	# startTime = datetime.now()
	# with open(cwd + "/embeddings", "rb") as my_file2:
	#     matrix_res = pickle.load(my_file2)

	# with open(cwd + "/norms", "rb") as my_file2:
	#     norms = pickle.load(my_file2)    

	# with open(cwd + "/output_tables/deep_learning_df", "rb") as my_file2:
	#     deep_learning_df = pickle.load(my_file2)
	# time_used = datetime.now() - startTime
	# print("Time used (load files): " + str(time_used))
	   
	# startTime = datetime.now()
	# grouped = deep_learning_df.groupby(["RETAILER_TYPE"])
	# time_used = datetime.now() - startTime
	# print("Time used (group by): " + str(time_used))

	startTime = datetime.now()
	for name, group_i in grouped:

	    index = group_i.index.values
	    # print(index)
	    number_of_products_2 = len(matrix_res)
	    idx_to_product = list(range(number_of_products_2))
	    # print(norms.shape)
	    # print(query_norm.shape)
	    norms = query_norm*norms.T
	    # print(norms.shape)
	    sim_idx = (query_res.dot(matrix_res.T))/norms
	    # sim_idx = (query_res.dot(matrix_res.T))

	    # print(matrix_res.shape)
	    # print(matrix_res[1])
	    # print(sim_idx.shape)
	    # print(sim_idx[0,:].shape)
	    # print(sim_idx[0,:])
	    # print(sim_idx[0,:][0])
	    # print(sim_idx[0,:][3564])
	    # print(sim_idx[0,:][2473])
	    # print(sim_idx[0,:][2107])

	    vs = []
	    url = []
	    score = []
	    # print(len(image_paths))
	    for q_i in range(len(image_paths)):
		    products_temp = search.top_k_products(
			    sim_idx[q_i,:], idx_to_product,
			    search_size, search_size_2, same_image_epsilon)
		    # print(products_temp)
		    products_2, sim_score = search.top_k_products_parser(
			    search_size_2, products_temp,
			    same_image_epsilon, sim_idx[q_i], index)
		    # print(products_2)
		    vs.append(deep_learning_df["VARIANT_ID"][products_2].tolist())
		    url.append(deep_learning_df["IMAGE_URL"][products_2].tolist())
		    score.append(sim_score)


	time_used = datetime.now() - startTime
	print("Time used (search): " + str(time_used))

	return vs, url, score