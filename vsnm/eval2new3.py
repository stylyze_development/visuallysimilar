import csv
import cv2
from datetime import datetime
import logging
import numpy as np
import os
import pandas as pd
import pickle
import scipy.io
import tensorflow as tf
# import tensorflow.compat.v1 as tf
# tf.disable_v2_behavior()
from PIL import Image 
from numpy import linalg as LA

from . import inferencenm as inference

# logging.basicConfig(level=logging.INFO)


def embedding(model_input_path):
    """

    """

    cwd = os.getcwd()

    image = tf.keras.preprocessing.image
    preprocess = tf.keras.applications.inception_v3.preprocess_input
    myinception = tf.keras.applications.inception_v3.InceptionV3(include_top=False,pooling='max')

    # Import the model input table
    with open(model_input_path, "rb") as my_file2:
        deep_learning_df_total = pickle.load(my_file2)

    no_image_counter = 0
    ratiler_type_dic = {}
    j = 0
    for key, value in deep_learning_df_total.items():

        ratiler_type_dic[key] = str(j)

        print("************************************")
        print(str(j))
        print(key)
        print(len(value))

        deep_learning_df = value
        grouped = deep_learning_df.groupby(["RETAILER_TYPE"])
         
        print("************************************")
        print("***NUMBER OF LSBS:" + str(len(deep_learning_df)))
        print("***NUMBER OF GROUPS:" + str(len(grouped)))
        print("************************************")

        for name, group_i in grouped:

            print(name)
            number_of_products = len(group_i)
            print(number_of_products)
            image_paths = group_i["CURATOR_IMAGE_PATH"].tolist() 

            # startTime = datetime.now()
            # matrix_res_final, norm_final = inference.inference(image_paths)
            # time_used = datetime.now() - startTime
            # print("Time used: " + str(time_used))

            matrix_res_final = np.zeros([number_of_products, 2048])
            norm_final = np.zeros([number_of_products, 1])
            i = 0
            startTime = datetime.now()
            for image_path_i in image_paths:
                try:
                    if (os.path.isfile(image_path_i)):
                        img = Image.open(image_path_i)
                        img = img.resize((229, 229))
                        mat = image.img_to_array(img)
                        mat = np.expand_dims(mat, axis=0)
                        aa = preprocess(mat)
                        itemvector = myinception.predict(aa)
                        norm_i = LA.norm(itemvector)
                        matrix_res_final[i:(i + 1) , :] = itemvector
                        norm_final[i:(i + 1) , :] = norm_i
                    else:
                        no_image_counter += 1
                        itemvector = np.zeros((1,2048))
                        norm_i = 1
                        matrix_res_final[i:(i + 1) , :] = itemvector
                        norm_final[i:(i + 1) , :] = norm_i
                    i += 1
                except:
                    no_image_counter += 1
                    itemvector = np.zeros((1,2048))
                    norm_i = 1
                    matrix_res_final[i:(i + 1) , :] = itemvector
                    norm_final[i:(i + 1) , :] = norm_i
                    i += 1
            time_used = datetime.now() - startTime
            print("Time used: " + str(time_used))

            with open(cwd + "/embeddings/" + "embeddings_" + str(j), "wb") as my_file:
                pickle.dump(matrix_res_final, my_file, protocol=2) 

            with open(cwd + "/norms/" + "norms_" + str(j), "wb") as my_file:
                pickle.dump(norm_final, my_file, protocol=2)

        j += 1

    print("# of No Images: " + str(no_image_counter))
    with open(cwd + "/ratiler_type_dic", "wb") as my_file:
        pickle.dump(ratiler_type_dic, my_file, protocol=2)    

                 
