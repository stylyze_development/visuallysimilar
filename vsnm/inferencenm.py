import csv
import cv2
from datetime import datetime
import logging
import numpy as np
import os
import pandas as pd
import pickle
import scipy.io
import tensorflow as tf
# import tensorflow.compat.v1 as tf
# tf.disable_v2_behavior()

from numpy import linalg as LA

from . import generatornew as generator
from . import modelsnew as models

def inference(image_paths):
    init = tf.global_variables_initializer()
    with tf.Session() as sess:

        embedding_size = 25088
        batch_size = 1
        output_layer = "relu5_4"

        sess.run(init)
        img_placeholder = tf.placeholder(tf.float32, shape=(None, 112, 112, 3))
        net_val, mean_pixel = models.vgg19_no_top("/home/tfuser/visuallysimilar/visually_similar_v1//weights/imagenet-vgg-verydeep-19.mat", img_placeholder)
        matrix_res_final = np.zeros([len(image_paths), embedding_size])
        norm_final = np.zeros([len(image_paths), 1])
        ii = 0
        for image_path in image_paths:

            image_path = [image_path]
            
            y_labels = np.zeros((1, 1))
            number_of_products = 1

            params = {'dim_x': 112,
                      'dim_y': 112,
                      'dim_z': 3,
                      'batch_size': 1,
                      'image_normalization': 250,
                      'n_y': 1,
                      'shuffle': False}

            y_labels_training = {}
            partition_training = []
            for i in range(number_of_products):
                index_i = i
                y_labels_training[str(index_i)] = y_labels[i, :]
                partition_training.append(str(index_i))
                training_generator = generator.DataGenerator(**params).generate(y_labels_training, partition_training, image_path)
                num_batches = np.int(np.round(number_of_products / batch_size))
                matrix_res = np.zeros([number_of_products, embedding_size])

                startTime = datetime.now()
                for i in range(num_batches):
                    (batch_X_training, batch_Y_training, bad_img) = next(training_generator)

                    train_features = net_val[output_layer].eval(feed_dict={img_placeholder: batch_X_training})
                    train_vectorized = np.ndarray((batch_size, embedding_size))
                    for j in range(batch_size):
                        curr_feat = train_features[j, :, :, :]
                        curr_feat_vec = np.reshape(curr_feat, (1, -1))
                        train_vectorized[j, :] = curr_feat_vec
                        matrix_res[i * batch_size:(i + 1) * batch_size, :] = train_vectorized
                        norm_i = LA.norm(matrix_res)

            
            matrix_res_final[ii:(ii + 1), :] = matrix_res
            norm_final[ii:(ii + 1), :] = norm_i
            ii += 1

    return matrix_res_final, norm_final

                     