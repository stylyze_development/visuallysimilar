import csv
import numpy as np
import logging
import os
import pandas as pd
import pickle
import re
import shutil

# TODO: implement some logging functions to replace the print functions

# logging.basicConfig(level=logging.INFO)


def load_data(database_path, column_names):
    """

    """

    # Load items in the database (big table)
    cwd = os.getcwd()
    df = pd.read_csv(cwd + database_path)

    print("************************************" + str(len(df)))

    # df = df.loc[:200000]

    # Rename the columns
    df = df.rename(columns={column_names[0]: "VARIANT_ID",
                            column_names[1]: "TYPES",
                            column_names[2]: "STYLES",
                            column_names[3]: "CURATOR_IMAGE_PATH",
                            column_names[4]: "IMAGE_URL",
                            column_names[5]: "CATEGORIES"
                            })

    # Pull the columns needed from the big table
    df = df[["VARIANT_ID", "TYPES", "STYLES", "CURATOR_IMAGE_PATH", "IMAGE_URL", "CATEGORIES", "ARCHIVED", "SHAPES", "PATTERNS", "COLOR_FINISH", "PROD_ID", "SPECIFICATIONS"]]
    df = df.fillna("Missing")
    # df = df[df["ARCHIVED"] != 1]
    # df = df[(df["CATEGORIES"] != "Missing") & (df["TYPES"] != "Missing")]
    print("************************************" + str(len(df)))

    # for i in range(len(df)):

    #     if df.iloc[i]["CATEGORIES"] == "Casual Dresses" or df.iloc[i]["CATEGORIES"] == "Daytime Mini Dresses" or df.iloc[i]["CATEGORIES"] == "Dresses" or df.iloc[i]["CATEGORIES"] == "Evening Dresses" or df.iloc[i]["CATEGORIES"] == "Work Dresses":   
    #         df["CATEGORIES"].iloc[i] = df["CATEGORIES"].iloc[i]
    #     else:
    #         df["CATEGORIES"].iloc[i] = "Missing"           

            
    df_big = df
    df_big = df_big[df_big["CATEGORIES"] == "Dresses"]
     

    print("NUMBER OF LSBS:" + str(len(df_big)))

    bad_image = 0
    lsb_list = []
    url_list = []
    id_list = []
    for i in range(len(df_big)):
        # print(df["CURATOR_IMAGE_PATH"].iloc[i])

        if os.path.isfile(df_big["CURATOR_IMAGE_PATH"].iloc[i]):
            shutil.copyfile(df_big["CURATOR_IMAGE_PATH"].iloc[i], "/opt/tomcat/blobs/fashioniq2/" + "r" + str(i) + ".jpg")
            lsb_list.append(df_big["VARIANT_ID"].iloc[i])
            url_list.append(df_big["IMAGE_URL"].iloc[i])
            id_list.append("r" + str(i))
        else:
            bad_image += 1

        # print(bad_image)  
        # print(len(lsb_list))
        # print(len(url_list))
        # print(len(id_list))  

        list_final = []
        for i in range((len(lsb_list))):
            list_final_i = [lsb_list[i], url_list[i], id_list[i]]
            list_final.append(list_final_i)
             
        table = pd.DataFrame(list_final, columns=["variant_id", "url", "id"])
        table.to_csv("/opt/tomcat/blobs/fashioniq2_list_final_20210310.csv")      

    cwd = os.getcwd()

     