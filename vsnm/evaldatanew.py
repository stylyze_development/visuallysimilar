import csv
import numpy as np
import logging
import os
import pandas as pd
import pickle
import re

# TODO: implement some logging functions to replace the print functions

# logging.basicConfig(level=logging.INFO)


def load_data(database_path, output_table_path, column_names):
    """

    """

    # Load items in the database (big table)
    cwd = os.getcwd()
    # df = pd.read_csv(cwd + database_path, engine='python', error_bad_lines=False)
    df = pd.read_csv(cwd + database_path)

    # print("************************************" + str(len(df)))

    # df = df.loc[:200000]

    # Rename the columns
    df = df.rename(columns={column_names[0]: "VARIANT_ID",
                            column_names[1]: "TYPES",
                            column_names[2]: "STYLES",
                            column_names[3]: "CURATOR_IMAGE_PATH",
                            column_names[4]: "IMAGE_URL",
                            column_names[5]: "CATEGORIES",
                            column_names[6]: "BRAND"
                            })

    # Pull the columns needed from the big table
    df = df[["OBJECT_ID", "VARIANT_ID", "TYPES", "STYLES", "CURATOR_IMAGE_PATH", "IMAGE_URL", "CATEGORIES", "BRAND", "ARCHIVED", "BVD", "RETAILER_TYPE"]]
    df = df.fillna("Missing")
    df = df[df["ARCHIVED"] != 1]
    # df = df[df["BVD"] != True]
    df = df[df["TYPES"] != "Missing"]

    df_total = {}
    grouped = df.groupby(["RETAILER_TYPE"])
     
    print("###########################################")
    print("***NUMBER OF GROUPS:" + str(len(grouped)))
    print("###########################################")

    for name, group_i in grouped:
        if name == "Jackets":
        # if len(group_i) >= 1:
            df_total[name] = group_i
    
    print("###########################################")
    print("# of RETAILER_TYPEs: " + str(len(df_total)))
    print("###########################################")

    # model_input_path = cwd + "/output_tables/deep_learning_df_total"
    # with open(model_input_path, "wb") as my_file:
    #     pickle.dump(df_total, my_file, protocol=2)

    # model_input_path_2 = cwd + "/output_tables/deep_learning_df"
    # with open(model_input_path_2, "wb") as my_file:
    #     pickle.dump(df, my_file, protocol=2)

    df_ = df_total["Jackets"]
    model_input_path = cwd + "/output_tables/deep_learning_df"
    with open(model_input_path, "wb") as my_file:
        pickle.dump(df_, my_file, protocol=2)

    return model_input_path 
