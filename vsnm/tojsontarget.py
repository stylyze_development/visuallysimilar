import numpy as np
import os
import pandas as pd
import pickle
import sys
import json


def output_json(output_table_path_3, output_table_path_12):

    cwd = os.getcwd()
    csv_path = output_table_path_3 + "id.csv"
    id_csv = pd.read_csv(cwd + csv_path)

    csv_path = output_table_path_3 + "score.csv"
    score_csv = pd.read_csv(cwd + csv_path)

    id_csv = id_csv.drop(["Unnamed: 0"], axis=1)
    print(id_csv.head())
    score_csv = score_csv.drop(["Unnamed: 0"], axis=1)
    print(score_csv.head())

    id_score = []
    for i in range(len(id_csv)):
        # for i in range(20):
        items = id_csv.loc[i]
        scores = score_csv.loc[i]

        list_items = []
        id_score_i = {}
        item_i = {}
        for j in range(21):
            if j == 0:
                id_score_i["mainVariantId"] = str(items[j])

            if (j > 0 and scores[j] > 0):
                item_i["variandId"] = str(items[j])
                item_i["confidenceLevel"] = scores[j]
                list_items.append(item_i)
                item_i = {}

        id_score_i["visuallySimilarItems"] = list_items
        id_score.append(id_score_i)

    print(id_score[:5])
    print(len(id_score))

    with open(cwd + output_table_path_12, 'w') as f:
        json.dump(id_score, f)

    # Output file path of id.csv
    # id.csv is the output file to the backend team
    # output_csv_path = `python evaluation.py input_csv_path`
    sys.stdout.write(cwd + output_table_path_12)
    sys.exit(0)
