import csv
import cv2
from datetime import datetime
import numpy as np
import os
import pandas as pd
import pickle
import scipy.io
import tensorflow as tf

# Use embeddings to rank similar products


def top_k_products(sim_idx, mapper, search_size, search_size_2, same_image_epsilon):

    # return [mapper[x] for x in np.argsort(sim_idx)[:-search_size - 1:-1]]
    list_temp = [mapper[x] for x in np.argsort(sim_idx)[:-search_size - 1:-1]]
    list_values = [sim_idx[x] for x in list_temp]

    for i in range(len(list_values)):
        for j in range(i + 1, len(list_values)):
            if i != len(list_values) - 1:
                if np.abs(list_values[j] - list_values[i]) < same_image_epsilon:
                    list_values[j] = -1

    return [list_temp[x] for x in np.argsort(list_values)[:-search_size_2 - 1:-1]]


def top_k_products_parser(search_size_2, products_temp, same_image_epsilon, sim_idx, index):

    products = products_temp[:search_size_2]

    products_2 = [-1] * search_size_2
    sim_score = [-1] * search_size_2
    j = 0
    for ii in products:
        products_2[j] = int(index[ii])
        sim_score[j] = sim_idx[ii]
        j += 1

    return products_2, sim_score
