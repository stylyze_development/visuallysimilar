import numpy as np
import os
import pandas as pd
import pickle
import sys


def output_lsbs(model_input_path, model_output_path, model_output_path_2, output_table_path_3):

    # Load the visually similar search result
    with open(model_output_path, "rb") as my_file:
        deep_learning_dic = pickle.load(my_file)

    # Load the visually similar search score result
    with open(model_output_path_2, "rb") as my_file2:
        deep_learning_score_dic = pickle.load(my_file2)

    # Load the big table as a reference to get the type, style, url, etc.
    with open(model_input_path, "rb") as my_file3:
        deep_learning_df = pickle.load(my_file3)

    output = {}
    output_score = {}

    # List of items contains the indexes for the seed items of interest for
    # debugging purpose
    # list_of_items = range(0, 5000)

    for key in deep_learning_dic.keys():
        for item_i in range(len(deep_learning_dic[key])):
            index = deep_learning_dic[key][str(item_i)][0]
            output[str(index)] = deep_learning_dic[key][str(item_i)]
            output_score[str(index)] = deep_learning_score_dic[
                key][str(item_i)]
            # if index in list_of_items:
            #     output[str(index)] = deep_learning_dic[key][str(item_i)]
            #     output_score[str(index)] = deep_learning_score_dic[
            #         key][str(item_i)]

    # Output url, index, variant id, type, style for the list of similar items
    output_2 = {}
    output_3 = []
    output_4 = []
    output_5 = {}
    output_6 = []
    output_7 = []
    output_8 = []
    output_9 = []
    output_10 = []
    for key in output.keys():
        output_2[key] = deep_learning_df["IMAGE_URL"][output[key]].tolist()
        output_3.append(output_2[key])
        output_4.append(output[key])
        output_5[key] = deep_learning_df["VARIANT_ID"][output[key]].tolist()
        output_6.append(output_5[key])
        output_9.append(output_score[key])
        
    # We can upload the csv files to google spreadsheet to visualize the result
    cwd = os.getcwd()
    table = pd.DataFrame(output_3, columns=[
                         '1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
                         '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21'])
    table.to_csv(cwd + output_table_path_3 + "url.csv")

    table_index = pd.DataFrame(
        output_4, columns=['1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
                           '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21'])
    table_index.to_csv(cwd + output_table_path_3 + "index.csv")

    table_id = pd.DataFrame(
        output_6, columns=['1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
                           '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21'])
    table_id.to_csv(cwd + output_table_path_3 + "id.csv")

     
    table_score = pd.DataFrame(
        output_9, columns=['1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
                           '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21'])
    table_score.to_csv(cwd + output_table_path_3 + "score.csv")

     