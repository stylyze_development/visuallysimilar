import sys

# TODO: use python argparse to manage the configuration file
# from command line

defaults_config = {"database_path": "/data_tables/Visually_Similar_bergdorfgoodman_qaApproved_qaUnapproved_extended_03102021.csv",
                   "column_names": ["VARIANT_ID", "TYPES", "STYLES", "CURATOR_IMAGE_PATH", "IMAGE_URL", "CATEGORIES"]}


def parse_arguments():
    args = defaults_config
    return args
