import csv
import numpy as np
import logging
import os
import pandas as pd
import pickle
import re

# TODO: implement some logging functions to replace the print functions

# logging.basicConfig(level=logging.INFO)


def load_data(database_path, output_table_path, column_names):
    """

    """

    # Load items in the database (big table)
    cwd = os.getcwd()
    df = pd.read_csv(cwd + database_path)

    print("************************************" + str(len(df)))

    # df = df.loc[:200000]

    # Rename the columns
    df = df.rename(columns={column_names[0]: "VARIANT_ID",
                            column_names[1]: "TYPES",
                            column_names[2]: "STYLES",
                            column_names[3]: "CURATOR_IMAGE_PATH",
                            column_names[4]: "IMAGE_URL",
                            column_names[5]: "CATEGORIES"
                            })

    # Pull the columns needed from the big table
    df = df[["VARIANT_ID", "TYPES", "STYLES", "CURATOR_IMAGE_PATH", "IMAGE_URL", "CATEGORIES", "ARCHIVED", "SHAPES", "PATTERNS", "COLOR_FINISH"]]
    df = df.fillna("Missing")
    # df = df[df["ARCHIVED"] != 1]
    print("************************************" + str(len(df)))

    def parser_color(str_input):
        if str_input != "Missing":
            a = re.split('[-]', str_input)
            return a[0]
        else:
            return "Missing" 

    df["COLOR_FINISH_2"] = df.apply(lambda row: parser_color(row["COLOR_FINISH"]), axis=1) 

    def parser_shape(str_input):
        if str_input != "Missing":
            if str_input == "Square":
                return "square"
            elif str_input == "Rectangle":
                return "rectangle"
            elif str_input == "Oval":
                return "oval"
            # if str_input == "Square":
            #     return "square"
            # if str_input == "Square":
            #     return "square"                
            else:
                return str_input
        else:
            return "Missing"
    
    df["SHAPES2"] = df.apply(lambda row: parser_shape(row["SHAPES"]), axis=1)

    for i in range(len(df)):

        if df.iloc[i]["TYPES"] != "Missing":

            if df.iloc[i]["STYLES"] == "Christmas":
                df["TYPES"].iloc[i] = df.iloc[i]["TYPES"] + ";" + df.iloc[i]["STYLES"]
            elif df.iloc[i]["STYLES"] == "Halloween":
                df["TYPES"].iloc[i] = df.iloc[i]["TYPES"] + ";" + df.iloc[i]["STYLES"]
            elif df.iloc[i]["STYLES"] == "Hanukkah":
                df["TYPES"].iloc[i] = df.iloc[i]["TYPES"] + ";" + df.iloc[i]["STYLES"]
            elif df.iloc[i]["STYLES"] == "Thanksgiving":
                df["TYPES"].iloc[i] = df.iloc[i]["TYPES"] + ";" + df.iloc[i]["STYLES"]
            elif df.iloc[i]["STYLES"] == "Easter":
                df["TYPES"].iloc[i] = df.iloc[i]["TYPES"] + ";" + df.iloc[i]["STYLES"] 

            elif df.iloc[i]["TYPES"] == "area rug":
                df["TYPES"].iloc[i] = df.iloc[i]["TYPES"] + ";" + df.iloc[i]["COLOR_FINISH_2"] + ";" + df.iloc[i]["SHAPES2"]  
            elif df.iloc[i]["TYPES"] == "kitchen rug":
                df["TYPES"].iloc[i] = df.iloc[i]["TYPES"] + ";" + df.iloc[i]["COLOR_FINISH_2"] + ";" + df.iloc[i]["SHAPES2"]  
            elif df.iloc[i]["TYPES"] == "outdoor area rug":
                df["TYPES"].iloc[i] = df.iloc[i]["TYPES"] + ";" + df.iloc[i]["COLOR_FINISH_2"] + ";" + df.iloc[i]["SHAPES2"]  
            elif df.iloc[i]["TYPES"] == "outdoor rugs":
                df["TYPES"].iloc[i] = df.iloc[i]["TYPES"] + ";" + df.iloc[i]["COLOR_FINISH_2"] + ";" + df.iloc[i]["SHAPES2"]  
            elif df.iloc[i]["TYPES"] == "round rug":
                df["TYPES"].iloc[i] = df.iloc[i]["TYPES"] + ";" + df.iloc[i]["COLOR_FINISH_2"] + ";" + df.iloc[i]["SHAPES2"] 
            elif df.iloc[i]["TYPES"] == "outdoor runner":
                df["TYPES"].iloc[i] = df.iloc[i]["TYPES"] + ";" + df.iloc[i]["COLOR_FINISH_2"] + ";" + df.iloc[i]["SHAPES2"] 
            elif df.iloc[i]["TYPES"] == "runner":
                df["TYPES"].iloc[i] = df.iloc[i]["TYPES"] + ";" + df.iloc[i]["COLOR_FINISH_2"] + ";" + df.iloc[i]["SHAPES2"]  
            elif df.iloc[i]["TYPES"] == "accent rug":
                df["TYPES"].iloc[i] = df.iloc[i]["TYPES"] + ";" + df.iloc[i]["COLOR_FINISH_2"] + ";" + df.iloc[i]["SHAPES2"]                            
            elif df.iloc[i]["TYPES"] == "decorative pillow":
                df["TYPES"].iloc[i] = df.iloc[i]["TYPES"] + ";" + df.iloc[i]["COLOR_FINISH_2"] + ";" + df.iloc[i]["SHAPES2"]   

        # df_big = df_big[(df_big["TYPES"] == "area rug") | (df_big["TYPES"] == "kitchen rug") | (df_big["TYPES"] == "outdoor area rug") |
    #                 (df_big["TYPES"] == "outdoor rugs") | (df_big["TYPES"] == "round rug") | (df_big["TYPES"] == "outdoor runner") |
    #                 (df_big["TYPES"] == "runner") | (df_big["TYPES"] == "accent rug")]     

        # elif df.iloc[i]["TYPES"] == "area rug" and df.iloc[i]["SHAPES"] == "rectangle" and df.iloc[i]["PATTERNS"] == "geometric":
        #     df["TYPES"].iloc[i] = df.iloc[i]["TYPES"] + ";" + df.iloc[i]["SHAPES"] + ";" + df.iloc[i]["PATTERNS"]
        #     # print("ITEM: " + str(i))
        #     # count_1 += 1
        # elif df.iloc[i]["TYPES"] == "area rug" and df.iloc[i]["SHAPES"] == "rectangle" and df.iloc[i]["PATTERNS"] == "floral":
        #     df["TYPES"].iloc[i] = df.iloc[i]["TYPES"] + ";" + df.iloc[i]["SHAPES"] + ";" + df.iloc[i]["PATTERNS"]  
        #     # print("ITEM: " + str(i))
        #     # count_2 += 1
        # elif df.iloc[i]["TYPES"] == "area rug" and df.iloc[i]["SHAPES"] == "rectangle" and df.iloc[i]["PATTERNS"] == "botanical":
        #     df["TYPES"].iloc[i] = df.iloc[i]["TYPES"] + ";" + df.iloc[i]["SHAPES"] + ";" + df.iloc[i]["PATTERNS"]
        #     # count_3 += 1
        # elif df.iloc[i]["TYPES"] == "area rug" and df.iloc[i]["SHAPES"] == "rectangle" and df.iloc[i]["PATTERNS"] == "Missing":
        #     df["TYPES"].iloc[i] = df.iloc[i]["TYPES"] + ";" + df.iloc[i]["SHAPES"] + ";" + df.iloc[i]["PATTERNS"]
        #     # count_4 += 1    
        # elif df.iloc[i]["TYPES"] == "area rug":
        #     df["TYPES"].iloc[i] = df.iloc[i]["TYPES"] + ";" + df.iloc[i]["SHAPES"]
        #     # count_5 += 1

    #     if df.iloc[i]["TYPES"] == "area rug" and df.iloc[i]["PATTERNS"] == "geometric":
    #         df["TYPES"].iloc[i] = df.iloc[i]["TYPES"] + ";" + df.iloc[i]["SHAPES"] + ";" + df.iloc[i]["PATTERNS"]
    #         # print("ITEM: " + str(i))
    #     elif df.iloc[i]["TYPES"] == "area rug" and df.iloc[i]["PATTERNS"] != "geometric":
    #         df["TYPES"].iloc[i] = df.iloc[i]["TYPES"] + ";" + df.iloc[i]["SHAPES"]  
    #         # print("ITEM: " + str(i))    

    # The following functions are used to parse the type and style attributes
    def parser(str_input):
        if str_input != "Missing":
            a = str_input.replace(" ", "")
            a = re.split('[;]', a)
            a.sort()
            a = ",".join(a)
            return a
        else:
            return "Missing"

    def parser_2(str_input):
        if str_input != "Missing":
            a = str_input.replace(" ", "")
            a = re.split('[;]', a)
            # a = a[0]
            a.sort()
            a = ",".join(a)
            return a
        else:
            return "Missing"

    # Apply the parsers to the types and styles
    df["TYPES"] = df.apply(lambda row: parser(row["TYPES"]), axis=1)
    df["STYLES"] = df.apply(lambda row: parser_2(row["STYLES"]), axis=1)
    # df["CATEGORIES"] = df.apply(lambda row: parser_2(row["CATEGORIES"]), axis=1)

    df_big = df
    # df_big["VARIANT_ID"] = df_big["VARIANT_ID"].astype('int64')

    # Drop the items with missing and/or uncategorized type and/or style
    # df_big = df_big[df_big["STYLES"] != "Missing"]
    df_big = df_big[df_big["TYPES"] != "Missing"]
    # df_big = df_big[df_big["CATEGORIES"] != "Missing"]
    # df_big = df_big[df_big["STYLES"] != "Uncategorized"]
    # df_big = df_big[df_big["TYPES"] != "uncategorized"]

    # df_big = df_big[df_big["TYPES"] == "Rectangle,arearug,gray"]
    

    print("NUMBER OF LSBS:" + str(len(df_big)))

    cwd = os.getcwd()

    model_input_path = cwd + output_table_path
    with open(model_input_path, "wb") as my_file:
        pickle.dump(df_big, my_file, protocol=2)

    return model_input_path
