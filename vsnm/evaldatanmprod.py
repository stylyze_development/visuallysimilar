import csv
import numpy as np
import logging
import os
import pandas as pd
import pickle
import re

# TODO: implement some logging functions to replace the print functions

# logging.basicConfig(level=logging.INFO)


def load_data(database_path, output_table_path, column_names):
    """

    """

    # Load items in the database (big table)
    cwd = os.getcwd()
    # df = pd.read_csv(cwd + database_path, engine='python', error_bad_lines=False)
    df = pd.read_csv(database_path)

    # print("************************************" + str(len(df)))

    # df = df.loc[:200000]

    # Rename the columns
    df = df.rename(columns={column_names[0]: "VARIANT_ID",
                            column_names[1]: "TYPES",
                            column_names[2]: "STYLES",
                            column_names[3]: "CURATOR_IMAGE_PATH",
                            column_names[4]: "IMAGE_URL",
                            column_names[5]: "CATEGORIES",
                            column_names[6]: "BRAND"
                            })

    # Pull the columns needed from the big table
    df = df[["OBJECT_ID", "VARIANT_ID", "TYPES", "STYLES", "CURATOR_IMAGE_PATH", "IMAGE_URL", "CATEGORIES", "BRAND", "ARCHIVED", "BVD", "RETAILER_TYPE"]]
    df = df.fillna("Missing")
    
    # df = df[df["ARCHIVED"] != 1]
    # df = df[df["RETAILER_TYPE"] == "Jackets"]
    # print(len(df))
    # df_big = df

    # The following functions are used to parse the type and style attributes
    def parser(str_input):
        if str_input != "Missing":
            a = str_input.replace(" ", "")
            a = re.split('[;]', a)
            a.sort()
            # a = ",".join(a)
            return a
        else:
            return "Missing"

    def parser_2(str_input):
        if str_input != "Missing":
            a = str_input.replace(" ", "")
            a = re.split('[;]', a)
            # a = a[0]
            a.sort()
            a = ",".join(a)
            return a
        else:
            return "Missing"

    # Apply the parsers to the types and styles
    df["TYPES"] = df.apply(lambda row: parser_2(row["TYPES"]), axis=1)
    df["STYLES"] = df.apply(lambda row: parser(row["STYLES"]), axis=1)
    df["CATEGORIES"] = df.apply(lambda row: parser_2(row["CATEGORIES"]), axis=1)  

    df_big = df
    # df_big["VARIANT_ID"] = df_big["VARIANT_ID"].astype('int64')

    # Drop the items with missing and/or uncategorized type and/or style
    # df_big = df_big[df_big["STYLES"] != "Missing"]
    # df_big = df_big[df_big["TYPES"] != "Missing"]
    # df_big = df_big[df_big["CATEGORIES"] != "Missing"]
    # df_big = df_big[df_big["STYLES"] != "Uncategorized"]
    # df_big = df_big[df_big["TYPES"] != "uncategorized"]

    # df_big = df_big[df_big["BRAND"] != "CHANEL"] 
    # df_big = df_big[df_big["BRAND"] != "Van Cleef & Arpels"]
    # df_big = df_big[df_big["BRAND"] != "Gucci"]

    count = 0
    for i in df_big["BRAND"]:
        if i == "CHANEL":
            count += 1
    print(count)

    count = 0
    for i in df_big["BRAND"]:
        if i == "Chanel":
            count += 1
    print(count)

    count = 0
    for i in df_big["BRAND"]:
        if i == "Gucci":
            count += 1
    print(count)

    count = 0
    for i in df_big["BRAND"]:
        if i == "GUCCI":
            count += 1
    print(count)

    # df = df.reset_index(drop=True)

    for i in range(len(df_big)):

        if df_big.iloc[i]["CATEGORIES"] == "Makeup" or df_big.iloc[i]["CATEGORIES"] == "Makeup Tools" or df_big.iloc[i]["CATEGORIES"] == "Skin Care" or df_big.iloc[i]["CATEGORIES"] == "Mens Cologne and Grooming" or df_big.iloc[i]["CATEGORIES"] == "Hair Care" or df_big.iloc[i]["CATEGORIES"] == "Nails":   
            df_big["CATEGORIES"].iloc[i] = df_big.iloc[i]["TYPES"] 

        if df_big.iloc[i]["TYPES"] == "pajamashorts" or df_big.iloc[i]["TYPES"] == "pajamapants" or df_big.iloc[i]["TYPES"] == "loungeshorts" or df_big.iloc[i]["TYPES"] == "loungepants":
            df_big["CATEGORIES"].iloc[i] = "Pajama Bottoms"
        elif df_big.iloc[i]["TYPES"] == "pajamatop" or df_big.iloc[i]["TYPES"] == "loungetop":
            df_big["CATEGORIES"].iloc[i] = "Pajama Tops" 
        elif df_big.iloc[i]["TYPES"] == "pajamaset" or df_big.iloc[i]["TYPES"] == "loungejumpsuit" or df_big.iloc[i]["TYPES"] == "loungeromper":
            df_big["CATEGORIES"].iloc[i] = "Pajama Sets" 
        elif df_big.iloc[i]["TYPES"] == "nightgown":
            df_big["CATEGORIES"].iloc[i] = "Nightgowns"
        elif df_big.iloc[i]["TYPES"] == "robe":
            df_big["CATEGORIES"].iloc[i] = "Robes"                

        if df_big.iloc[i]["CATEGORIES"] == "Missing":
            df_big["CATEGORIES"].iloc[i] = df_big.iloc[i]["TYPES"] 

        if df_big.iloc[i]["CATEGORIES"] == "Tops":  
            # print(df.iloc[i]["STYLES"][0])
            df_big["CATEGORIES"].iloc[i] = df_big.iloc[i]["CATEGORIES"] + ";" + df_big.iloc[i]["STYLES"][0]

        if df_big.iloc[i]["CATEGORIES"] == "Bags":  
            # print(df.iloc[i]["STYLES"][0])
            df_big["CATEGORIES"].iloc[i] = df_big.iloc[i]["CATEGORIES"] + ";" + df_big.iloc[i]["STYLES"][0]
            
        if df_big.iloc[i]["CATEGORIES"] == "Earrings":  
            # print(df.iloc[i]["STYLES"][0])
            df_big["CATEGORIES"].iloc[i] = df_big.iloc[i]["CATEGORIES"] + ";" + df_big.iloc[i]["STYLES"][0]

        if df_big.iloc[i]["CATEGORIES"] == "Dresses":  
            # print(df.iloc[i]["STYLES"][0])
            df_big["CATEGORIES"].iloc[i] = df_big.iloc[i]["CATEGORIES"] + ";" + df_big.iloc[i]["STYLES"][0]

        if df_big.iloc[i]["CATEGORIES"] == "Blouses":  
            # print(df.iloc[i]["STYLES"][0])
            df_big["CATEGORIES"].iloc[i] = df_big.iloc[i]["CATEGORIES"] + ";" + df_big.iloc[i]["STYLES"][0]

        if df_big.iloc[i]["CATEGORIES"] == "HomeAccents":  
            # print(df.iloc[i]["STYLES"][0])
            df_big["CATEGORIES"].iloc[i] = df_big.iloc[i]["CATEGORIES"] + ";" + df_big.iloc[i]["STYLES"][0]

        if df_big.iloc[i]["CATEGORIES"] == "Necklaces":  
            # print(df.iloc[i]["STYLES"][0])
            df_big["CATEGORIES"].iloc[i] = df_big.iloc[i]["CATEGORIES"] + ";" + df_big.iloc[i]["STYLES"][0]             

    df_big = df_big[df_big["CATEGORIES"] != "Missing"]        

    # print("NUMBER OF LSBS:" + str(len(df_big)))

    cwd = os.getcwd()

    model_input_path = cwd + output_table_path
    with open(model_input_path, "wb") as my_file:
        pickle.dump(df_big, my_file, protocol=2)

    return model_input_path
