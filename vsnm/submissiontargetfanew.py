import numpy as np
import os
import pandas as pd
import pickle
import sys
from datetime import datetime
import copy


def output_lsbs(model_input_path, model_output_path, model_output_path_2, output_table_path_3):

    # Load the visually similar search result
    with open(model_output_path, "rb") as my_file:
        deep_learning_dic = pickle.load(my_file)

    # Load the visually similar search score result
    with open(model_output_path_2, "rb") as my_file2:
        deep_learning_score_dic = pickle.load(my_file2)

    # Load the big table as a reference to get the type, style, url, etc.
    with open(model_input_path, "rb") as my_file3:
        deep_learning_df = pickle.load(my_file3)

    check_id = deep_learning_df[deep_learning_df["VARIANT_ID"] == "77550097-116904-Brown"]
    print(check_id)
    print(check_id["CATEGORIES"])
    check_id = deep_learning_df[deep_learning_df["VARIANT_ID"] == "77550097-116904-Green"]
    print(check_id)
    print(check_id["CATEGORIES"])
    check_id = deep_learning_df[deep_learning_df["VARIANT_ID"] == "77642047-116904-Brown"]
    print(check_id)
    print(check_id["CATEGORIES"])
    check_id = deep_learning_df[deep_learning_df["VARIANT_ID"] == "77642047-116904-Gray"]
    print(check_id)
    print(check_id["CATEGORIES"])
    # print(check_id["CATEGORIES"])
    # check_id = deep_learning_df[deep_learning_df["VARIANT_ID"] == "77642047-116904-Brown"]
    # print(check_id["CURATOR_IMAGE_PATH"])
    # print(check_id["TYPES"])
    # check_id = deep_learning_df[deep_learning_df["VARIANT_ID"] == "77642047-116904-Gray"]
    # print(check_id["CURATOR_IMAGE_PATH"])
    # print(check_id["TYPES"])
     

    output = {}
    output_score = {}

    # List of items contains the indexes for the seed items of interest for
    # debugging purpose
    # list_of_items = range(0, 5000)
    # list_of_items = [7850,7503,4071,3894]

    i = 0
    for key in deep_learning_dic.keys():
        # i += 1
        # if i % 100 == 0:
        #     print(i)
        #     print(key)
        for item_i in range(len(deep_learning_dic[key])):
            index = deep_learning_dic[key][str(item_i)][0]
            output[str(index)] = deep_learning_dic[key][str(item_i)]
            output_score[str(index)] = deep_learning_score_dic[
                key][str(item_i)]
            # if index in list_of_items:
            #     output[str(index)] = deep_learning_dic[key][str(item_i)]
            #     output_score[str(index)] = deep_learning_score_dic[key][str(item_i)]

    # Output url, index, variant id, type, style for the list of similar items
    output_2 = {}
    output_3 = []
    output_4 = []
    output_5 = {}
    output_6 = []
    output_7 = []
    output_8 = []
    output_9 = []
    output_10 = []
    print(len(output.keys()))
    print("*****************")
    ii = 0
    startTime = datetime.now()
    for key in output.keys():
         
        ii += 1
        if ii % 1000 == 0:
            time_used = datetime.now() - startTime
            print(ii)
            print(time_used)

        color_list = deep_learning_df["COLOR_FINISH"][output[key]].tolist()
        prod_id_list = deep_learning_df["PROD_ID"][output[key]].tolist() 

        # print(output[key])
        # print(output_score[key])
        # print(color_list)
        # print(prod_id_list) 

        # output_copy = copy.deepcopy(output[key])
        # output_score_copy = copy.deepcopy(output_score[key])  

        # print("******")
        # print(len(output[key]))
        # print(len(output_score[key]))
        # print("*************")

        for i in range(len(prod_id_list)):
            for j in range(i + 1, len(prod_id_list)):
                if i != len(prod_id_list) - 1:
                    if prod_id_list[j] == prod_id_list[i] and color_list[j] == color_list[i]:
                        # output_copy.remove(output[key][j])  
                        # output_score_copy.remove(output_score[key][j])
                        output[key][j] = -1
                        output_score[key][j] = -1 
        score_index = list(np.argsort(output_score[key][1:])[::-1]) 
        output_tail = list(np.array(output[key][1:])[score_index])[:20]
        output_score_tail = list(np.array(output_score[key][1:])[score_index])[:20]

        output[key] = [output[key][0]] + output_tail 
        output_score[key] = [output_score[key][0]] + output_score_tail
        # print(score_index)
        # print(output[key])              

        # if len(output_copy) < 21:
        #     output_copy = output_copy + [-1]*(21-len(output_copy))
        #     output_score_copy = output_score_copy + [-1]*(21-len(output_score_copy))
        # elif len(output_copy) > 21:
        #     output_copy = output_copy[:21]
        #     output_score_copy = output_score_copy[:21]
                
        # output[key] = output_copy
        # output_score[key] = output_score_copy   

        # print("******")
        # print(output[key])
        # print(output_score[key])
        # print("*************")                

        output_2[key] = deep_learning_df["IMAGE_URL"][output[key]].tolist()
        output_3.append(output_2[key])
        output_4.append(output[key])
        output_5[key] = deep_learning_df["VARIANT_ID"][output[key]].tolist()
        output_6.append(output_5[key])
        output_7.append(deep_learning_df["TYPES"][output[key]].tolist()[0])
        output_8.append(deep_learning_df["STYLES"][output[key]].tolist()[0])
        output_9.append(output_score[key])
        output_10.append(deep_learning_df["CATEGORIES"][output[key]].tolist()[0])

    # print(output_6)    

    # We can upload the csv files to google spreadsheet to visualize the result
    cwd = os.getcwd()
    table = pd.DataFrame(output_3, columns=['1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
                         '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21'])
                         # '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32',
                         # '33', '34', '35', '36', '37', '38', '39', '40', '41', '42', '43',
                         # '44', '45', '46', '47', '48', '49', '50', '51'])
    table.to_csv(cwd + output_table_path_3 + "url.csv")

    table_index = pd.DataFrame(
        output_4, columns= ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
                            '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21'])
    table_index.to_csv(cwd + output_table_path_3 + "index.csv")

    table_id = pd.DataFrame(
        output_6, columns= ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
                            '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21']) 
    table_id.to_csv(cwd + output_table_path_3 + "id.csv")

    table_type = pd.DataFrame(output_7, columns=['TYPE'])
    table_type.to_csv(cwd + output_table_path_3 + "type.csv")

    table_style = pd.DataFrame(output_8, columns=['STYLE'])
    table_style.to_csv(cwd + output_table_path_3 + "style.csv")

    table_score = pd.DataFrame(
        output_9, columns= ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
                            '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21']) 

    table_score.to_csv(cwd + output_table_path_3 + "score.csv")

    table_categories = pd.DataFrame(output_10, columns=['CATEGORY'])
    table_categories.to_csv(cwd + output_table_path_3 + "category.csv")

    print(output_6[0])

    output_color_value = []
    for i in range(len(output_6)):
        output_color_value_i = []
        for j in range(len(output_6[i])):
            id_value = output_6[i][j]
            color_value = deep_learning_df[deep_learning_df["VARIANT_ID"] == id_value]["COLOR_FINISH"].values.tolist()
            if len(color_value) >= 1:
                output_color_value_i.append(color_value[0])
            else:
                output_color_value_i.append("NULL")
        output_color_value.append(output_color_value_i) 

    table_color_value = pd.DataFrame(
        output_color_value, columns= ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
                            '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21']) 

    table_color_value.to_csv(cwd + output_table_path_3 + "color_value.csv")                               

    # Output file path of id.csv
    # id.csv is the output file to the backend team
    # output_csv_path = `python evaluation.py input_csv_path`
    # sys.stdout.write(cwd + output_table_path_3 + "id.csv")
    # sys.exit(0)
