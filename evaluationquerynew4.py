import csv
import numpy as np
import pandas as pd
import pickle
import re
from datetime import datetime
import os
import json
import time

import vsnm.querynew3 as query
import vsnm.modelsnew as models

import tensorflow as tf

cwd = os.getcwd()


def main():
	startTime = datetime.now()
	image = tf.keras.preprocessing.image
	# preprocess = tf.keras.applications.VGG19.preprocess_input
	# myinception = tf.keras.applications.VGG19(include_top=False, pooling='max')
	preprocess = tf.keras.applications.inception_v3.preprocess_input
	myinception = tf.keras.applications.inception_v3.InceptionV3(include_top=False, pooling='max')
	time_used = datetime.now() - startTime
	print("Time used (open tensorflow): " + str(time_used))

	startTime = datetime.now()
	with open(cwd + "/output_tables/deep_learning_df_total", "rb") as my_file2:
		deep_learning_df_total = pickle.load(my_file2)
	with open(cwd + "/ratiler_type_dic", "rb") as my_file2:
		ratiler_type_dic = pickle.load(my_file2)
	time_used = datetime.now() - startTime
	print("Time used (load input tables): " + str(time_used))

	startTime = datetime.now()
	matrix_res_all = {}
	norms_all = {}
	for key_type in ratiler_type_dic:
		index_i = ratiler_type_dic[key_type]
		with open(cwd + "/embeddings/embeddings_" + index_i, "rb") as my_file2:
			matrix_res_all[index_i] = pickle.load(my_file2)
		with open(cwd + "/norms/norms_" + index_i, "rb") as my_file2:
			norms_all[index_i] = pickle.load(my_file2)
		
	time_used = datetime.now() - startTime
	print("Time used (load embeddings): " + str(time_used))

	while (True):

		while (os.path.isfile("/opt/tomcat/blobs/export/nm/files/test_request.json")):
			startTime1 = datetime.now()
			time.sleep(.200)
			with open("/opt/tomcat/blobs/export/nm/files/test_request.json") as json_file:
				image_pathss = json.load(json_file)
			time_used = datetime.now() - startTime1
			print("******************************************")
			print("Time used (load requests): " + str(time_used))

			vs = []
			url = []
			score = []
			stype = []
			scate = []
			variant_id = []
			image_path_counter = 0
			for image_path in image_pathss:
				startTime = datetime.now()
				input_ratiler_type = image_pathss[image_path_counter].split(",")[0]
				deep_learning_df = deep_learning_df_total[input_ratiler_type]
				type_index = ratiler_type_dic[input_ratiler_type]

				matrix_res = matrix_res_all[type_index]
				norms = norms_all[type_index]

				time_used = datetime.now() - startTime
				print("Time used (load embeddings (lsb request)): " + str(time_used)) 

				image_paths = [image_path.split(",")[1]]
				variant_id_i = [image_path.split(",")[2]]
				startTime = datetime.now()
				grouped = deep_learning_df.groupby(["RETAILER_TYPE"])
				time_used = datetime.now() - startTime
				print("Time used (group by): " + str(time_used))

				vs_i, url_i, score_i, stype_i, scate_i = query.query(image_paths, matrix_res, norms, deep_learning_df, grouped, image, preprocess, myinception)
				# print(vs_i)
				# print(score_i)
				vs += vs_i
				url += url_i
				score += score_i
				stype += stype_i
				scate += scate_i
				variant_id += variant_id_i

				image_path_counter += 1

			id_score = []
			for i in range(len(vs)):
				items = vs[i]
				scores = score[i]

				id_score_i = {}
				id_score_i["mainVariantId"] = variant_id[i]

				list_items = []
				item_i = {}
				for j in range(20):
					if (scores[j] > 0):
						item_i["variandId"] = items[j]
						item_i["confidenceLevel"] = scores[j]
						list_items.append(item_i)
						item_i = {}

				id_score_i["visuallySimilarItems"] = list_items
				id_score.append(id_score_i)

			# print(id_score[0])
			# print(len(id_score))
			print(vs[0][:5])
			print(stype[0][:5])
			print(scate[0][:5])
			print(score[0][:5])

			with open("/opt/tomcat/blobs/export/nm/files/" + "nm_new_arrival_id_score_05032021.json", 'w') as f:
				json.dump(id_score, f)

			table = pd.DataFrame(vs, columns=[
								 '1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
								 '11', '12', '13', '14', '15', '16', '17', '18', '19', '20'])
			table.to_csv("/home/tfuser/visuallysimilar/visually_similar_v1/output_tables/" + "id.csv")
			table = pd.DataFrame(url, columns=[
								 '1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
								 '11', '12', '13', '14', '15', '16', '17', '18', '19', '20'])
			table.to_csv("/home/tfuser/visuallysimilar/visually_similar_v1/output_tables/" + "url.csv")

			os.remove("/opt/tomcat/blobs/export/nm/files/test_request.json")
			
			time_used = datetime.now() - startTime1
			print("Time used (total): " + str(time_used))

	 
if __name__ == '__main__':
	main()